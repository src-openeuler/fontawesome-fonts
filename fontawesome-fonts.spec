%global fontname fontawesome
%global __provides_exclude ^font\\(:lang=[a-z]{2,3}(-[a-z]{2,3})?\\)$

Name:           fontawesome-fonts
Version:        6.7.2
Release:        1
Summary:        The iconic font and CSS toolkit
License:        OFL-1.1 AND MIT
URL:            https://github.com/FortAwesome/Font-Awesome
Source0:        https://github.com/FortAwesome/Font-Awesome/releases/download/%{version}/fontawesome-free-%{version}-desktop.zip
Source1:        https://github.com/FortAwesome/Font-Awesome/releases/download/%{version}/fontawesome-free-%{version}-web.zip

BuildArch:      noarch
BuildRequires:  fontpackages-devel unzip
Requires:       fontpackages-filesystem

Provides:       %{name}-web = %{version}-%{release}
Obsoletes:      %{name}-web < %{version}-%{release} %{name}-help < %{version}-%{release}

%description
Font Awesome gives you scalable vector icons that can instantly be
customized — size, color, drop shadow, and anything that can be done with the
power of CSS.

%prep
%setup -q -c
%setup -q -T -D -a 1
mv fontawesome-free-%{version}-desktop/LICENSE.txt .

%build

%install
install -m 0755 -d %{buildroot}%{_fontdir}
install -p -m 0644 */otfs/*.otf %{buildroot}%{_fontdir}

install -m 0755 -d %{buildroot}%{_datadir}/font-awesome-web/webfonts
install -p -m 0644 */webfonts/*.{ttf,woff2} %{buildroot}%{_datadir}/font-awesome-web/webfonts
cp -pr */css */less */scss %{buildroot}%{_datadir}/font-awesome-web/

%files
%license LICENSE.txt
%defattr(-,root,root)
%{_datadir}/font-awesome-web/
%{_datadir}/fonts/fontawesome/*

%changelog
* Mon Jan 13 2025 wangkai <13474090681@163.com> - 6.7.2-1
- Update to 6.7.2

* Thu Jun 13 2024 yao_xin <yao_xin001@hoperun.com> - 4.7.0-9
- License compliance rectification

* Mon Sep 14 2020 Ge Wang <wangge20@huawei.com> - 4.7.0-8
- Modify Source0 Url

* Fri Feb 14 2020 yanzhihua <yanzhihua4@huawei.com> - 4.7.0-7
- Package init
